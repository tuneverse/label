package usecases

import (
	"context"
	"label/internal/consts"
	"label/internal/entities"
	"label/internal/repo"
	"strconv"

	"gitlab.com/tuneverse/toolkit/core/logger"

	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// LabelUseCases represents use cases for handling Label-related operations.
type LabelUseCases struct {
	repo repo.LabelRepoImply
}

// LabelUseCaseImply is an interface for the Label use case implementation.
// LabelUseCaseImply defines the interface for the LabelUseCases.
type LabelUseCaseImply interface {
	CreateLabel(context.Context, entities.Label) error
	Validation(context.Context, entities.Label) (map[string][]string, error)
	ViewAllLabel(context.Context, entities.Paramater) ([]entities.Label, models.MetaData, error)
	UpdateLabel(context.Context, string, string, string, map[string]interface{}) error
	DeleteLabel(context.Context, string, string, string, map[string]interface{}) error
	ViewAllReleases(context.Context, string, string, string) ([]map[string]any, entities.MetaData, error)
}

// NewLabelUseCases creates a new instance of LabelUseCases.
// NewLabelUseCases creates a new instance of LabelUseCases.
func NewLabelUseCases(labelRepo repo.LabelRepoImply) LabelUseCaseImply {
	return &LabelUseCases{
		repo: labelRepo,
	}
}

// CreateLabel calls the repository to create a label
func (label *LabelUseCases) CreateLabel(ctx context.Context, labelInfo entities.Label) error {

	return label.repo.CreateLabel(ctx, labelInfo)
}

// Validation performs validation checks on the label information
func (label *LabelUseCases) Validation(ctx context.Context, labelInfo entities.Label) (map[string][]string, error) {

	fieldsMap := map[string][]string{}
	var err error

	log := logger.Log().WithContext(ctx)
	//check Member id exist or not
	isMemberIDExist, err := label.repo.IsMemberExist(ctx, labelInfo.MemberId)
	if err != nil {
		log.Errorf("Validation failed, IsMemberExist failed, Error = %s", err.Error())
		return nil, err
	}
	if !isMemberIDExist {
		log.Errorf("Validation failed, IsMemberExist failed, Member does not exist")
		utils.AppendValuesToMap(fieldsMap, "common_message", "not_exist")
		return fieldsMap, nil
	}

	// Check if the label name is valid
	if valid := utils.IsEmpty(labelInfo.Name); valid {
		utils.AppendValuesToMap(fieldsMap, "name", "required")
		return fieldsMap, nil
	}

	//IsValidName returns a error value even if we need validation error
	// Check if the label name exceeds the maximum length
	err, isNameValid := utils.IsValidName(labelInfo.Name)

	if err != nil || !isNameValid {
		utils.AppendValuesToMap(fieldsMap, "name", "invalid_name_length")
		return fieldsMap, nil
	}

	// Check if the label name exists in the blacklist
	isBlackListed, err := label.repo.IsBlackListName(ctx, labelInfo.Name)
	if err != nil {
		log.Errorf("Validation failed, IsBlackListName failed, Error = %s", err.Error())
		return nil, err
	}
	if isBlackListed {
		log.Errorf("Validation failed, IsBlackListName failed, Label name exists in blacklist")
		utils.AppendValuesToMap(fieldsMap, "name", "exists_in_blacklist")
		return fieldsMap, nil
	}

	//check label name is already exists or not
	isNameUnique, err := label.repo.IsNameUnique(ctx, labelInfo)
	if err != nil {
		log.Errorf("Validation failed, IsNameUnique failed, Error = %s", err.Error())
		return nil, err
	}
	if isNameUnique {
		log.Errorf("Validation failed, IsNameUnique failed")
		utils.AppendValuesToMap(fieldsMap, "name", "not_unique")
		return fieldsMap, nil
	}

	return fieldsMap, err
}

// ViewAllLabel retrieves a paginated list of labels based on limit, offset, and ID.
func (label *LabelUseCases) ViewAllLabel(ctx context.Context, params entities.Paramater) ([]entities.Label, models.MetaData, error) {

	if params.Limit <= 0 {
		params.Limit = 10
	}

	if params.Page <= 0 {
		params.Page = 1
	}
	labelData, metaData, err := label.repo.ViewAllLabel(ctx, params)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("ViewAllLabel failed, Error = %s", err.Error())
		return labelData, metaData, err
	}
	return labelData, metaData, nil
}

// ViewAllLabel retrieves a paginated list of releases  based on limit, offset, and ID.
func (label *LabelUseCases) ViewAllReleases(ctx context.Context, limit string, pageStr string, id string) ([]map[string]any, entities.MetaData, error) {

	pageSize, _ := strconv.Atoi(limit)
	pageNumber, _ := strconv.Atoi(pageStr)

	if !(pageSize > 0) {
		pageSize, _ = strconv.Atoi(consts.DefaultLimitStr)
	}
	if !(pageNumber > 0) {
		pageNumber, _ = strconv.Atoi(consts.DefaultPageStr)
	}

	labelData, metaData, err := label.repo.ViewAllReleases(ctx, pageSize, pageNumber, id)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("ViewAllReleases failed, Invalid JSON data, Error = %s", err.Error())
		return labelData, metaData, err
	}
	return labelData, metaData, nil
}

// updateLabel to update details in label
func (lb *LabelUseCases) UpdateLabel(ctx context.Context, labelId string, memberId string, partnerId string, label map[string]interface{}) error {
	return lb.repo.UpdateLabel(ctx, labelId, memberId, partnerId, label)
}

// DeleteLabel is used to logically delete a label.
func (lb *LabelUseCases) DeleteLabel(ctx context.Context, labelId string, memberId string, partnerId string, label map[string]interface{}) error {
	return lb.repo.DeleteLabel(ctx, labelId, memberId, partnerId, label)
}
