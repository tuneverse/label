package usecases

import (
	"context"
	"database/sql"
	"errors"
	"label/internal/consts"
	"label/internal/entities"
	"label/internal/repo/mock"
	"strconv"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
)

func TestDeleteLabel_Success(t *testing.T) {
	// Create a new instance of the GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock instance of the LabelRepoImply interface
	mockLabelRepo := mock.NewMockLabelRepoImply(ctrl)

	labelRepo := NewLabelUseCases(mockLabelRepo)

	// Set up expected input parameters
	ctx := context.Background()
	labelID := "label123"
	memberID := "member456"
	partnerID := "partner789"
	labelData := map[string]interface{}{} // You can pass label data if needed

	// Set up the behavior of the mock for the DeleteLabel method
	mockLabelRepo.EXPECT().
		DeleteLabel(ctx, labelID, memberID, partnerID, labelData).
		Return(nil) // No error for success

	// Call the DeleteLabel function
	err := labelRepo.DeleteLabel(ctx, labelID, memberID, partnerID, labelData)

	// Verify there is no error
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
}

func TestDeleteLabel_Error(t *testing.T) {
	// Create a new instance of the GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock instance of the LabelRepoImply interface
	mockLabelRepo := mock.NewMockLabelRepoImply(ctrl)

	labelRepo := NewLabelUseCases(mockLabelRepo)

	// Set up expected input parameters
	ctx := context.Background()
	labelID := "label123"
	memberID := "member456"
	partnerID := "partner789"
	labelData := map[string]interface{}{} // You can pass label data if needed

	// Set up the behavior of the mock for the DeleteLabel method to return an error
	expectedError := errors.New("an error occurred")
	mockLabelRepo.EXPECT().
		DeleteLabel(ctx, labelID, memberID, partnerID, labelData).
		Return(expectedError)

	// Call the DeleteLabel function
	err := labelRepo.DeleteLabel(ctx, labelID, memberID, partnerID, labelData)

	// Verify that the error matches the expected error
	if err != expectedError {
		t.Errorf("Expected error to be %v, but got %v", expectedError, err)
	}
}

func TestDeleteLabel_DeleteError(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockLabelRepo := mock.NewMockLabelRepoImply(ctrl)

	labelRepo := NewLabelUseCases(mockLabelRepo)

	ctx := context.Background()
	labelID := "deleteErrorLabel123" // Simulate an error during label deletion
	memberID := "member456"
	partnerID := "partner789"
	labelData := map[string]interface{}{} // You can pass label data if needed

	// Mock that an error occurs during label deletion
	expectedError := errors.New("an error occurred during deletion")
	mockLabelRepo.EXPECT().
		DeleteLabel(ctx, labelID, memberID, partnerID, labelData).
		Return(expectedError)

	err := labelRepo.DeleteLabel(ctx, labelID, memberID, partnerID, labelData)

	if err != expectedError {
		t.Errorf("Expected error to be %v, but got %v", expectedError, err)
	}
}

// TestCreateLabel to check CreateLabel function
func TestCreateLabel(t *testing.T) {
	//	uuidStr := "550e8400-e29b-41d4-a716-446655440000"

	// Parse the string to a UUID
	//u, _ := uuid.Parse(uuidStr)
	labelInfo := entities.Label{
		ID:        uuid.New(),
		Name:      "sample",
		IsActive:  true,
		IsDeleted: false,
	}
	// Define test data
	testCases := []struct {
		name          string
		buildStubs    func(store *mock.MockLabelRepoImply)
		checkResponse func(t *testing.T, err error)
	}{
		{
			name: "Successful creation", //success label creation
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					CreateLabel(gomock.Any(), labelInfo).
					Times(1).
					Return(nil)
			},
			checkResponse: func(t *testing.T, err error) {
				require.Nil(t, err)
			},
		},
		{
			name: "Error during label creation",
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					CreateLabel(gomock.Any(), labelInfo).
					Times(1).
					Return(errors.New("Error occured"))
			},
			checkResponse: func(t *testing.T, err error) {
				require.Error(t, err)
			},
		},
	}

	for i := range testCases {
		testCase := testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			//create a new mock controller for each testcase
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			//create mock store
			store := mock.NewMockLabelRepoImply(ctrl)
			testCase.buildStubs(store)
			labelUseCases := NewLabelUseCases(store)

			err := labelUseCases.CreateLabel(context.Background(), labelInfo)
			//check the response
			testCase.checkResponse(t, err)
		})
	}
}

// TestValidation to check Validation function
func TestValidation(t *testing.T) {

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: true,           // Include response data in logs.
	}
	// Check if the application is in debug mode.

	_ = logger.InitLogger(clientOpt)

	labelInfo := entities.Label{
		ID:        uuid.New(),
		Name:      "sample",
		IsActive:  true,
		IsDeleted: false,
		MemberId:  "member123",
	}

	// Define test data
	testCases := []struct {
		name          string
		buildStubs    func(store *mock.MockLabelRepoImply)
		checkResponse func(t *testing.T, fieldsMap map[string][]string, err error)
	}{
		{
			name: "Valid Label", // Valid label information
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					IsMemberExist(gomock.Any(), labelInfo.MemberId).
					Times(1).
					Return(true, nil)
				store.EXPECT().
					IsBlackListName(gomock.Any(), labelInfo.Name).
					Times(1).
					Return(false, nil)
				store.EXPECT().
					IsNameUnique(gomock.Any(), labelInfo).
					Times(1).
					Return(false, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Nil(t, err)
				require.Empty(t, fieldsMap)
			},
		},

		{
			name: "Invalid Member ID",
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					IsMemberExist(gomock.Any(), labelInfo.MemberId).
					Times(1).
					Return(false, nil)

				// Use gomock.Any() for IsBlackListName to indicate it's not expected to be called
				store.EXPECT().
					IsBlackListName(gomock.Any(), labelInfo.Name).
					Times(0).
					AnyTimes()

				// Use gomock.Any() for IsNameUnique to indicate it's not expected to be called
				store.EXPECT().
					IsNameUnique(gomock.Any(), gomock.Any()).
					Times(0).
					AnyTimes()
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Empty(t, err)
				require.Contains(t, fieldsMap, "common_message")
				require.Equal(t, []string{"not_exist"}, fieldsMap["common_message"])
			},
		},

		{
			name: "Blacklisted Name",
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					IsMemberExist(gomock.Any(), labelInfo.MemberId).
					Times(1).
					Return(true, nil)
				store.EXPECT().
					IsBlackListName(gomock.Any(), labelInfo.Name).
					Times(1).
					Return(true, nil)
				store.EXPECT().
					IsNameUnique(gomock.Any(), gomock.Any()).
					Times(0).
					AnyTimes()
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Empty(t, err)
				require.Contains(t, fieldsMap, "name")
				require.Equal(t, []string{"exists_in_blacklist"}, fieldsMap["name"])
			},
		},

		{
			name: "Not Unique Name",
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					IsMemberExist(gomock.Any(), labelInfo.MemberId).
					Times(1).
					Return(true, nil)
				store.EXPECT().
					IsBlackListName(gomock.Any(), labelInfo.Name).
					Times(1).
					Return(false, nil)
				store.EXPECT().
					IsNameUnique(gomock.Any(), labelInfo).
					Times(1).
					Return(true, nil) // Return false for non-unique name
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Empty(t, err)
				require.Contains(t, fieldsMap, "name")
				require.Equal(t, []string{"not_unique"}, fieldsMap["name"])
			},
		},
	}

	for i := range testCases {
		testCase := testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			// Create a new mock controller for each test case
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			ctx := context.Background()
			// ctxt := ctx.Request.Context()
			// Create a mock store
			store := mock.NewMockLabelRepoImply(ctrl)
			testCase.buildStubs(store)
			labelUseCases := NewLabelUseCases(store)

			fieldsMap, err := labelUseCases.Validation(ctx, labelInfo)

			// Check the response
			testCase.checkResponse(t, fieldsMap, err)
		})
	}
}

// TestViewAllLabel to check ViewAllLabel function
func TestViewAllLabel(t *testing.T) {

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: true,           // Include response data in logs.
	}
	// Check if the application is in debug mode.

	_ = logger.InitLogger(clientOpt)

	testCaseLimit := 5
	labels := make([]entities.Label, testCaseLimit)
	for i := 0; i < testCaseLimit; i++ {
		labels[i] = entities.Label{
			ID:        uuid.New(),
			Name:      "sample",
			IsActive:  true,
			IsDeleted: false,
		}
	}

	//define test cases
	testCases := []struct {
		name          string
		query         entities.Paramater
		buildStubs    func(store *mock.MockLabelRepoImply)
		checkresponse func(t *testing.T, labels []entities.Label, metaData models.MetaData, err error)
	}{
		{
			name: "Test case 1",
			query: entities.Paramater{
				Page:  1,
				Limit: 5,
			},
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					ViewAllLabel(gomock.Any(), gomock.Any()).
					Times(1).
					Return(labels, models.MetaData{
						CurrentPage: 1,
					}, nil)
			},
			checkresponse: func(t *testing.T, labelResponse []entities.Label, metaData models.MetaData, err error) {
				require.Nil(t, err)
				require.Equal(t, len(labels), len(labelResponse))
			},
		},
		{
			name: "Test case 2",
			query: entities.Paramater{
				MemberID: uuid.New().String(),
				Page:     1,
				Limit:    5,
				Value:    "sa",
			},
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					ViewAllLabel(gomock.Any(), gomock.Any()).
					Times(1).
					Return(labels, models.MetaData{
						CurrentPage: 1,
					}, nil)
			},
			checkresponse: func(t *testing.T, labelResponse []entities.Label, metaData models.MetaData, err error) {
				require.Nil(t, err)
				require.Equal(t, len(labels), len(labelResponse))
			},
		},
		{
			name: "Page number negative value",
			query: entities.Paramater{
				Page:  -1,
				Limit: 5,
			},
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					ViewAllLabel(gomock.Any(), gomock.Any()).
					Times(1).
					Return(labels, models.MetaData{
						CurrentPage: 1,
					}, nil)
			},
			checkresponse: func(t *testing.T, labelResponse []entities.Label, metaData models.MetaData, err error) {
				require.Nil(t, err)
				require.Equal(t, len(labels), len(labelResponse))
			},
		},
		{
			name: "Limit negative value",
			query: entities.Paramater{
				Page:  1,
				Limit: -5,
				Value: "AS",
			},
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					ViewAllLabel(gomock.Any(), gomock.Any()).
					Times(1).
					Return(labels, models.MetaData{
						CurrentPage: 1,
					}, nil)
			},
			checkresponse: func(t *testing.T, labelResponse []entities.Label, metaData models.MetaData, err error) {
				require.Nil(t, err)
				require.Equal(t, len(labels), len(labelResponse))
			},
		},

		{
			name: "Server error",
			query: entities.Paramater{
				Limit: 10,
			},
			buildStubs: func(store *mock.MockLabelRepoImply) {
				store.EXPECT().
					ViewAllLabel(gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil, models.MetaData{}, sql.ErrConnDone)
			},
			checkresponse: func(t *testing.T, labelResponse []entities.Label, metaData models.MetaData, err error) {
				require.Error(t, err)
			},
		},
	}

	for i := range testCases {
		testCase := testCases[i]
		// Run the testcases
		t.Run(testCase.name, func(t *testing.T) {
			//create a new mock controller for each testcase
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			//create mock store
			store := mock.NewMockLabelRepoImply(ctrl)
			testCase.buildStubs(store)
			labelUseCases := NewLabelUseCases(store)
			//resp, metadata, err := labelUseCases.ViewAllLabel(context.Background(), testCase.query.Limit, testCase.query.PageNumber, testCase.query.MemberID, testCase.query.Value)

			resp, metadata, err := labelUseCases.ViewAllLabel(context.Background(), entities.Paramater{
				Limit:    testCase.query.Limit,
				Page:     testCase.query.Page,
				MemberID: testCase.query.MemberID,
				Value:    testCase.query.Value,
			})

			//check the response
			testCase.checkresponse(t, resp, metadata, err)
		})
	}
}

func TestViewAllReleases_Success(t *testing.T) {
	// Create a new instance of the GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock instance of the LabelRepoImply interface
	mockLabelRepo := mock.NewMockLabelRepoImply(ctrl)

	labelUseCases := NewLabelUseCases(mockLabelRepo)

	expectedLimit := 10
	expectedPageNumber := 1
	expectedLabelID := "83940200-0331-420a-87f2-8ec4d140a239"

	expectedReleases := []map[string]any{
		{
			"id":               "8c72dca1-4a76-47a1-afec-5e0b08a31b54",
			"name":             "Release 1",
			"status":           "Pending",
			"releaseStartDate": "2023-09-06",
			"createdOn":        "2023-09-06T00:00:00Z",
		},
		{
			"id":               "d52549a3-6e12-4b0d-b949-6f3f2cd9b21f",
			"name":             "Release 2",
			"status":           "Published",
			"releaseStartDate": "2023-09-07",
			"createdOn":        "2023-09-07T00:00:00Z",
		},
	}
	expectedMetaData := entities.MetaData{
		Total:       2,
		PerPage:     10,
		CurrentPage: 1,
		Next:        0,
		Prev:        0,
	}

	var expectedError error

	mockLabelRepo.EXPECT().
		ViewAllReleases(gomock.Any(), expectedLimit, expectedPageNumber, expectedLabelID).
		Return(expectedReleases, expectedMetaData, expectedError)
	expectedLimitStr := strconv.Itoa(expectedLimit)
	expectedPageNumberStr := strconv.Itoa(expectedPageNumber)

	// Call the ViewAllReleases function
	releases, metaData, err := labelUseCases.ViewAllReleases(context.Background(), expectedLimitStr, expectedPageNumberStr, expectedLabelID)

	if err != expectedError {
		t.Errorf("Expected error to be %v, but got %v", expectedError, err)
	}

	if len(releases) != len(expectedReleases) {
		t.Errorf("Expected %d releases, but got %d", len(expectedReleases), len(releases))
	}

	for i, release := range releases {
		expectedRelease := expectedReleases[i]

		if release["id"] != expectedRelease["id"] {
			t.Errorf("Expected ID %s, but got %s", expectedRelease["id"], release["id"])
		}

		if release["name"] != expectedRelease["name"] {
			t.Errorf("Expected Name %s, but got %s", expectedRelease["name"], release["name"])
		}

	}

	if metaData != expectedMetaData {
		t.Errorf("Expected metadata to be %+v, but got %+v", expectedMetaData, metaData)
	}
}

func TestEditLabel_Success(t *testing.T) {
	// Create a new instance of the GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock instance of the LabelRepoImply interface
	mockLabelRepo := mock.NewMockLabelRepoImply(ctrl)

	labelUseCases := NewLabelUseCases(mockLabelRepo)

	// Set up expected input parameters
	ctx := context.Background()
	labelID := "label123"
	memberID := "member456"
	partnerID := "partner789"
	labelData := map[string]interface{}{
		"name":        "Updated Label",
		"description": "Updated Description",
	}

	// Set up the behavior of the mock for the EditLabel method
	mockLabelRepo.EXPECT().
		UpdateLabel(ctx, labelID, memberID, partnerID, labelData).
		Return(nil) // No error for success

	// Call the EditLabel function
	err := labelUseCases.UpdateLabel(ctx, labelID, memberID, partnerID, labelData)

	// Verify there is no error
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
}

func TestEditLabel_Error(t *testing.T) {
	// Create a new instance of the GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock instance of the LabelRepoImply interface
	mockLabelRepo := mock.NewMockLabelRepoImply(ctrl)

	labelUseCases := NewLabelUseCases(mockLabelRepo)

	// Set up expected input parameters
	ctx := context.Background()
	labelID := "label123"
	memberID := "member456"
	partnerID := "partner789"
	labelData := map[string]interface{}{
		"name":        "Updated Label",
		"description": "Updated Description",
	}

	// Set up a negative scenario with an expected error
	expectedError := errors.New("an error occurred")

	// Set up the behavior of the mock for the EditLabel method to return an error
	mockLabelRepo.EXPECT().
		UpdateLabel(ctx, labelID, memberID, partnerID, labelData).
		Return(expectedError)

	// Call the EditLabel function for the negative scenario
	err := labelUseCases.UpdateLabel(ctx, labelID, memberID, partnerID, labelData)

	// Verify the error matches the expected error
	if err != expectedError {
		t.Errorf("Expected error to be %v, but got %v", expectedError, err)
	}
}
