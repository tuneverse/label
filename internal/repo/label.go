package repo

import (
	"context"
	"database/sql"
	"label/internal/entities"
	"strings"
	"time"

	"fmt"
	"strconv"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuneverse/toolkit/core/logger"

	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// LabelRepo is responsible for handling label-related data.
type LabelRepo struct {
	db *sql.DB
}

// LabelRepoImply is an interface for the Label repository implementation.
type LabelRepoImply interface {
	UpdateLabel(context.Context, string, string, string, map[string]interface{}) error
	CreateLabel(context.Context, entities.Label) error
	IsNameUnique(context.Context, entities.Label) (bool, error)
	IsBlackListName(ctx context.Context, name string) (bool, error)
	IsMemberExist(ctx context.Context, id string) (bool, error)
	ViewAllLabel(context.Context, entities.Paramater) ([]entities.Label, models.MetaData, error)
	ViewAllReleases(context.Context, int, int, string) ([]map[string]any, entities.MetaData, error)
	DeleteLabel(context.Context, string, string, string, map[string]interface{}) error
}

// NewLabelRepo creates a new instance of LabelRepo.
func NewLabelRepo(db *sql.DB) LabelRepoImply {
	return &LabelRepo{db: db}
}

// CreateLabel insert a new label in to database
func (label *LabelRepo) CreateLabel(ctx context.Context, labelInfo entities.Label) error {

	insertQuery := `INSERT INTO content_supplier(name, ddex_party_id, member_id, partner_id) 
					VALUES ($1, $2, $3, $4)`

	_, err := label.db.ExecContext(ctx, insertQuery, labelInfo.Name, labelInfo.DdexPartyId, labelInfo.MemberId, labelInfo.PartnerId)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("CreateLabel failed, Error = %s", err.Error())
		return err
	}
	return nil
}

// IsNameUnique function to check label name is already exists or not
func (label *LabelRepo) IsNameUnique(ctx context.Context, labelInfo entities.Label) (bool, error) {

	var isUnique bool
	query := `SELECT EXISTS (SELECT 1 FROM content_supplier WHERE name ILIKE  $1 AND member_id = $2)`

	if err := label.db.QueryRowContext(ctx, query, labelInfo.Name, labelInfo.MemberId).Scan(&isUnique); err != nil {
		logger.Log().WithContext(ctx).Errorf("Validation failed, IsNameUnique failed, Error = %s", err.Error())
		return false, err
	}

	return isUnique, nil
}

// IsBlackListName function to check label name is already exists in blacklist words or not
func (label *LabelRepo) IsBlackListName(ctx context.Context, name string) (bool, error) {

	var isNameBlacklisted bool

	query := `SELECT EXISTS (SELECT 1 FROM artist_blacklist WHERE key_word ILIKE $1)`

	if err := label.db.QueryRowContext(ctx, query, name).Scan(&isNameBlacklisted); err != nil {
		logger.Log().WithContext(ctx).Errorf("Validation failed, IsBlackListName failed, Error = %s", err.Error())
		return false, err
	}
	return isNameBlacklisted, nil
}

// IsMemberExist function to check member is valid or not
func (label *LabelRepo) IsMemberExist(ctx context.Context, memberID string) (bool, error) {

	log := logger.Log().WithContext(ctx)
	var isMemberActive bool
	id, err := uuid.Parse(memberID)
	if err != nil {
		log.Errorf("Validation failed, IsMemberExist failed, Error in parse member id, Error = %s", err.Error())
		return false, err
	}

	query := `SELECT EXISTS (SELECT 1 FROM member WHERE id = $1 AND is_active = true)`

	if err := label.db.QueryRowContext(ctx, query, id).Scan(&isMemberActive); err != nil {
		log.Errorf("Validation failed, IsMemberExist failed, Error = %s", err.Error())
		return false, err
	}

	return isMemberActive, nil
}

// ViewAllLabel retrieves a paginated list of labels
func (label *LabelRepo) ViewAllLabel(ctx context.Context, params entities.Paramater) ([]entities.Label, models.MetaData, error) {

	offset := (params.Page - 1) * params.Limit
	memberID := params.MemberID
	var (
		labels     []entities.Label
		totalCount int
		args       []interface{}
	)

	query := "SELECT id, name, is_active, is_deleted, COUNT(id) OVER() as total_records FROM content_supplier"
	if len(params.Value) > 0 || memberID != "" {
		query += " WHERE"
	}
	if len(params.Value) > 0 {
		query += " name ILIKE $1"
		args = append(args, params.Value+"%")
	}
	if len(params.Value) > 0 && memberID != "" {
		query += " AND"
	}
	if memberID != "" {
		query += " member_id = $" + strconv.Itoa(len(args)+1)
		args = append(args, params.MemberID)
	}

	query = fmt.Sprintf("%s %s ", query, "	GROUP BY id, name")
	query = fmt.Sprintf("%s LIMIT $%d OFFSET $%d", query, len(args)+1, len(args)+2)
	args = append(args, params.Limit, offset)

	log := logger.Log().WithContext(ctx)

	rows, err := label.db.QueryContext(ctx, query, args...)
	if err != nil {
		log.Errorf("ViewAllLabel failed , QueryContext, Error = %s", err.Error())
		return nil, models.MetaData{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var labelInfo entities.Label
		err := rows.Scan(&labelInfo.ID, &labelInfo.Name, &labelInfo.IsActive, &labelInfo.IsDeleted, &totalCount)

		if err != nil {
			log.Errorf("ViewAllLabel failed , rows scan function ,Error = %s", err.Error())
			return nil, models.MetaData{}, err
		}

		labels = append(labels, labelInfo)
	}

	if err := rows.Err(); err != nil {
		log.Errorf("ViewAllLabel failed , Error = %s", err.Error())
		return nil, models.MetaData{}, err
	}

	metaData := models.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(params.Limit),
		CurrentPage: int32(params.Page),
		Next:        0,
		Prev:        0,
	}

	metaDataResult := utils.MetaDataInfo(&metaData)

	return labels, *metaDataResult, nil
}

func (label *LabelRepo) ViewAllReleases(ctx context.Context, limit, pageNumber int, labelId string) ([]map[string]any, entities.MetaData, error) {

	offset := (pageNumber - 1) * limit

	query := `
        SELECT id, name, status, release_start_date, created_on
        FROM product
        WHERE content_supplier_id = $1
        LIMIT $2 OFFSET $3
    `
	// COALESCE(release_start_date, '2021-12-01')

	var releaseSd sql.NullString
	// Execute the query and retrieve the results
	rows, err := label.db.QueryContext(ctx, query, labelId, limit, offset)
	if err != nil {
		return nil, entities.MetaData{}, err
	}
	defer rows.Close()

	// Define a slice to store the results
	var releases []map[string]any

	// Process the rows and populate the releases slice
	for rows.Next() {
		var (
			id               string // Change the data type to string
			name             string
			status           int
			releaseStartDate string
			createdOn        time.Time
		)

		// Scan the row into the variables
		if err := rows.Scan(&id, &name, &status, &releaseSd, &createdOn); err != nil {
			return nil, entities.MetaData{}, err
		}

		if releaseSd.Valid {
			releaseStartDate = releaseSd.String
		} else {
			releaseStartDate = ""
		}

		// Create a map for each release and populate it with data
		releaseInfo := map[string]any{
			"id":               id,
			"name":             name,
			"status":           status,
			"releaseStartDate": releaseStartDate,
			"createdOn":        createdOn.Format(time.RFC3339), // Format the time as needed
			// Add other fields as needed
		}

		// Append the release info map to the releases slice
		releases = append(releases, releaseInfo)
	}

	// Check for errors during row processing
	if err := rows.Err(); err != nil {
		return nil, entities.MetaData{}, err
	}

	// Calculate the total count of releases (you may need to modify this query)
	totalCount, err := label.calculateReleaseCount(ctx, labelId)
	if err != nil {
		return nil, entities.MetaData{}, err
	}

	// Create metadata
	metaData := entities.MetaData{
		Total:       totalCount,
		PerPage:     limit,
		CurrentPage: pageNumber,
		Next:        calculateNextPage(pageNumber, limit, totalCount),
		Prev:        calculatePrevPage(pageNumber),
	}

	return releases, metaData, nil
}

// Define a function to calculate the next page number
func calculateNextPage(currentPage, perPage, totalCount int) int {
	nextPage := currentPage + 1
	if nextPage*perPage > totalCount {
		return 0
	}
	return nextPage
}

// Define a function to calculate the previous page number
func calculatePrevPage(currentPage int) int {
	if currentPage <= 1 {
		return 0
	}
	return currentPage - 1
}

// Define a function to calculate the total count of releases (modify this query)
func (label *LabelRepo) calculateReleaseCount(ctx context.Context, labelId string) (int, error) {
	// Modify this query to count the releases for the given content_supplier_id
	countQuery := "SELECT COUNT(id) FROM product WHERE content_supplier_id = $1"

	var totalCount int

	if err := label.db.QueryRowContext(ctx, countQuery, labelId).Scan(&totalCount); err != nil {
		logger.Log().WithContext(ctx).Errorf("calculateReleaseCount failed, Error = %s", err.Error())

		return 0, err
	}
	return totalCount, nil
}

// UpdateLable to update a label
func (repo *LabelRepo) UpdateLabel(ctx context.Context, labelId string, memberId string, partnerId string, labelData map[string]interface{}) error {
	log := logrus.New()

	// Initialize slices and counter for building query
	var updates []string
	var values []interface{}
	counter := 1

	// Iterate through labelData to construct updates and values
	for field, value := range labelData {
		updates = append(updates, fmt.Sprintf("%s=$%d", field, counter))
		values = append(values, value)
		counter++
	}
	// Add member_id and partner_id to values
	values = append(values, memberId)
	values = append(values, partnerId)
	// Append labelId to values
	values = append(values, labelId)

	// SQL query to update label details
	query := fmt.Sprintf(`
    UPDATE content_supplier 
    SET %s
    WHERE  member_id=$%d AND partner_id=$%d AND id=$%d`, strings.Join(updates, ","), counter, counter+1, counter+2)

	// Execute the query with context
	_, err := repo.db.ExecContext(ctx, query, values...)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("UpdateLabel failed, Error = %s", err.Error())
		log.Errorf("Error updating user data: %v", err)
		return err
	}

	return nil
}

// DeleteLabel is used to logically delete a label.
func (repo *LabelRepo) DeleteLabel(ctx context.Context, labelID string, memberID string, partnerID string, labelData map[string]interface{}) error {

	log := logrus.New()

	// Check label already inactive
	var isActive bool

	query := fmt.Sprintf(`
    SELECT EXISTS(
        SELECT 1 FROM content_supplier
        WHERE id = $1 AND member_id = $2 AND partner_id = $3 AND is_active IN ('%s')
    )`, "true")

	// Execute the query to check if the label is active
	err := repo.db.QueryRowContext(ctx, query, labelID, memberID, partnerID).Scan(&isActive)
	if err != nil {
		log.Errorf("failed to fetch data: %v", err)
		return err
	}
	if !isActive {
		log.Errorf("label already inactive")
		return fmt.Errorf("label already inactive")
	}

	// Construct the query to update label status to deleted and inactive
	query = `
		UPDATE content_supplier
		SET is_deleted = $1, is_active = false
		WHERE id = $2 AND member_id = $3 AND partner_id = $4
		`

	// Execute the query with context to mark the label as deleted and inactive
	_, err = repo.db.ExecContext(ctx, query, true, labelID, memberID, partnerID)
	if err != nil {
		log.Errorf("error deleting app data: %v", err)
		return err
	}
	return nil
}
