package repo_test

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"label/internal/repo"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestViewAllReleases_Success(t *testing.T) {
	// Create a new mock database and sqlmock instance
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	// Create a LabelRepo with the mock database
	labelRepo := repo.NewLabelRepo(db)

	// Set up expected data and results
	expectedLimit := 10
	expectedPageNumber := 1
	expectedLabelID := "83940200-0331-420a-87f2-8ec4d140a239"

	// Define the expected rows
	expectedRows := sqlmock.NewRows([]string{"id", "name", "status", "release_start_date", "created_on"}).
		AddRow("8c72dca1-4a76-47a1-afec-5e0b08a31b54", "Release 1", 1, "2023-09-06", time.Now()).
		AddRow("d52549a3-6e12-4b0d-b949-6f3f2cd9b21f", "Release 2", 2, "2023-09-07", time.Now())

	// Expected query
	expectedQuery := `SELECT id, name, status, release_start_date, created_on FROM product WHERE content_supplier_id = \$1 LIMIT \$2 OFFSET \$3`

	// Set the expectations for the query
	mock.ExpectQuery(expectedQuery).
		WithArgs(expectedLabelID, expectedLimit, (expectedPageNumber-1)*expectedLimit).
		WillReturnRows(expectedRows)

	expectedCountQuery := `SELECT COUNT\(id\) FROM product WHERE content_supplier_id = \$1`

	expectedCountArgs := []driver.Value{expectedLabelID}

	// Set the expectations for the count query
	mock.ExpectQuery(expectedCountQuery).
		WithArgs(expectedCountArgs...).
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(2)) // Adjust the count as needed

	// Call the ViewAllReleases function
	releases, metaData, err := labelRepo.ViewAllReleases(context.Background(), expectedLimit, expectedPageNumber, expectedLabelID)
	if err != nil {
		t.Fatalf("Error calling ViewAllReleases: %v", err)
	}

	// Verify the results
	if len(releases) != 2 {
		t.Errorf("Expected 2 releases, but got %d", len(releases))
	}

	// Add more assertions based on the expected data
	if releases[0]["name"] != "Release 1" {
		t.Errorf("Expected name 'Release 1', but got %s", releases[0]["name"])
	}

	if metaData.Total != 2 {
		t.Errorf("Expected total count 2, but got %d", metaData.Total)
	}

	// Ensure all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Unfulfilled expectations: %s", err)
	}
}
func TestViewAllReleases_Error(t *testing.T) {
	// Create a new mock database and sqlmock instance
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	// Create a LabelRepo with the mock database
	labelRepo := repo.NewLabelRepo(db)

	// Set up expected data and results
	expectedLimit := 10
	expectedPageNumber := 1
	expectedLabelID := "your_label_id"

	// Define the expected query
	expectedQuery := `SELECT id, name, status, release_start_date, created_on FROM product WHERE content_supplier_id = \$1 LIMIT \$2 OFFSET \$3`

	// Define the expected args
	expectedArgs := []driver.Value{expectedLabelID, expectedLimit, (expectedPageNumber - 1) * expectedLimit}

	// Simulate an error by returning an error from the mock query
	mock.ExpectQuery(expectedQuery).
		WithArgs(expectedArgs...).
		WillReturnError(sql.ErrNoRows) // Simulate a "no rows" error

	// Call the ViewAllReleases function
	_, _, err = labelRepo.ViewAllReleases(context.Background(), expectedLimit, expectedPageNumber, expectedLabelID)
	if err == nil {
		t.Errorf("Expected an error, but got nil")
	}

	// Ensure all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Unfulfilled expectations: %s", err)
	}
}

func TestUpdateLabel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error occurred while creating mock database: %v", err)
	}
	defer db.Close()

	labelRepo := repo.NewLabelRepo(db)

	ctx := context.Background()
	labelID := "123e4567-e89b-12d3-a456-426614174000"
	memberID := "98765432-abcdef-9876-5432-abcdef123456"
	partnerID := "abcdef12-3456-7890-abcdef12345678"
	labelData := map[string]interface{}{
		"name":      "Updated Label",
		"ddexParty": "Updated DDEX",
	}

	// Expect the UPDATE query
	mock.ExpectExec("^UPDATE content_supplier").WithArgs(labelData["name"], labelData["ddexParty"], memberID, partnerID, labelID).
		WillReturnResult(sqlmock.NewResult(0, 1))

	err = labelRepo.UpdateLabel(ctx, labelID, memberID, partnerID, labelData)
	assert.NoError(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteLabel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error occurred while creating mock database: %v", err)
	}
	defer db.Close()

	labelRepo := repo.NewLabelRepo(db)

	ctx := context.Background()
	labelID := "123e4567-e89b-12d3-a456-426614174000"
	memberID := "98765432-abcdef-9876-5432-abcdef123456"
	partnerID := "abcdef12-3456-7890-abcdef12345678"

	mock.ExpectQuery("^SELECT EXISTS").WithArgs(labelID, memberID, partnerID).WillReturnRows(sqlmock.NewRows([]string{"exists"}).AddRow(true))

	mock.ExpectExec("^UPDATE content_supplier").WithArgs(true, labelID, memberID, partnerID).WillReturnResult(sqlmock.NewResult(0, 1))

	err = labelRepo.DeleteLabel(ctx, labelID, memberID, partnerID, nil)
	assert.NoError(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
