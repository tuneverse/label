package entities

// EnvConfig represents the configuration structure for the application.
type EnvConfig struct {
	Debug                  bool     `default:"true" split_words:"true"`
	Port                   int      `default:"8080" split_words:"true"`
	Db                     Database `split_words:"true"`
	AcceptedVersions       []string `required:"true" split_words:"true"`
	MigrationPath          string   `split_words:"true"`
	LocalisationServiceURL string   ` split_words:"true"`
	LoggerServiceURL       string   `split_words:"true"`
	LoggerSecret           string   `split_words:"true"`
	EndpointURL            string   ` split_words:"true"` // new change
	ResetLink              string   `split_words:"true"`
}

// Database represents the database configuration for the application.
type Database struct {
	User     string
	Password string
	Port     int
	Host     string
	DATABASE string
	Schema   string
	// Schema    string `envconfig:"default=public"`
	MaxActive int
	MaxIdle   int
}

// MetaData represents pagenation struct
type MetaData struct {
	Total       int `json:"total,omitempty"`
	PerPage     int `json:"per_page,omitempty"`
	CurrentPage int `json:"current_page,omitempty"`
	Next        int `json:"next,omitempty"`
	Prev        int `json:"prev,omitempty"`
}
