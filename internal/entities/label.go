package entities

import "github.com/google/uuid"

// Label represent the label information
type Label struct {
	ID          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	DdexPartyId string    `json:"ddex_party_id"`
	MemberId    string    `json:"member_id"`
	PartnerId   string    `json:"partner_id"`
	IsActive    bool      `json:"is_active"`
	IsDeleted   bool      `json:"is_deleted"`
}
type ErrorResponse struct {
	Message   string                 `json:"message"`
	ErrorCode interface{}            `json:"errorCode"`
	Errors    map[string]interface{} `json:"errors"`
}

// DataItem represents an item of data
type DataItem struct {
	URL      string `json:"URL"`
	Method   string `json:"Method"`
	Endpoint string `json:"Endpoint"`
}

// ResponseData struct represents the response data structure,
type ResponseData struct {
	Data []DataItem `json:"data"`
}

// Paramater represents query parameters for requests
type Paramater struct {
	Limit    int    `form:"limit"`
	Page     int    `form:"page"`
	Value    string `form:"value"`
	MemberID string `form:"member_id"`
}
