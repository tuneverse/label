package middlewares

import (
	"label/internal/entities"

	"github.com/gin-gonic/gin"
)

// Middlewares represents a collection of middleware functions for the application.
type Middlewares struct {
	Cfg *entities.EnvConfig
}

// NewMiddlewares creates a new Middlewares instance with the provided configuration.
func NewMiddlewares(cfg *entities.EnvConfig) *Middlewares {
	return &Middlewares{
		Cfg: cfg,
	}
}

// JwtDecoding is a middleware that decodes JWT tokens and sets default values for member ID and partner ID.
func (m Middlewares) JwtDecoding() gin.HandlerFunc {
	return func(c *gin.Context) {

		// default values for member id and partner id
		c.Set("member_id", "980e783c-e664-452d-b1ff-30d2e7767023")
		c.Set("partner_id", "614608f2-6538-4733-aded-96f902007254")
		c.Next()
	}
}
