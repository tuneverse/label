package controllers

import (
	"label/internal/consts"
	"label/internal/entities"
	"label/internal/usecases"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	toolkitconsts "gitlab.com/tuneverse/toolkit/consts"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// LabelController represents a controller responsible for handling Label-related API requests.
type LabelController struct {
	router   *gin.RouterGroup
	useCases usecases.LabelUseCaseImply
}

// NewLabelController creates a new instance of LabelController.
func NewLabelController(router *gin.RouterGroup, labelUseCase usecases.LabelUseCaseImply) *LabelController {
	return &LabelController{
		router:   router,
		useCases: labelUseCase,
	}
}

// InitRoutes initializes and configures the label-related routes for the LabelController.
func (label *LabelController) InitRoutes() {

	// Health handler
	label.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, label, "HealthHandler")
	})

	// Label creation
	label.router.POST("/:version/labels", func(ctx *gin.Context) {
		version.RenderHandler(ctx, label, "CreateLabel")
	})

	//view label details using memberid
	label.router.GET("/:version/labels", func(ctx *gin.Context) {
		version.RenderHandler(ctx, label, "ViewAllLabel")
	})

	label.router.GET("/:version/labels/:label_id/releases", func(ctx *gin.Context) {
		version.RenderHandler(ctx, label, "ViewAllReleases")
	})
	label.router.DELETE("/:version/labels/:id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, label, "DeleteLabel")
	})
	label.router.PATCH("/:version/labels/:id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, label, "UpdateLabel")
	})
}

// HealthHandler is a handler responsible for handling health check requests.
func (label *LabelController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

// CreateLabel retrieves and returns label information.
func (label *LabelController) CreateLabel(ctx *gin.Context) {

	var labelInfo entities.Label
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)

	if err := ctx.ShouldBindJSON(&labelInfo); err != nil {
		log.Errorf("CreateLabel failed, Invalid JSON data, Error = %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"code":    400,
			"error":   "Invalid request",
			"message": "binding error",
			"help":    "",
		})
		return
	}

	//Todo : To get partnerid and memberid from token

	memberID := ctx.GetString("MemberId")
	partnerID := ctx.GetString("PartnerId")
	labelInfo.MemberId = memberID
	labelInfo.PartnerId = partnerID

	if memberID == "" || partnerID == "" {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Context values not found or empty"})
		return
	}

	method := strings.ToLower(ctx.Request.Method)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	if !isEndpointExists {
		log.Errorf("CreateLabel failed, Invalid endpoint,%s", contextEndpoints)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	contextError, contextStatusError := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !contextStatusError {
		log.Errorf("CreateLabel failed, %s", contextError)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.ContextErr,
			"errors":    nil,
		})
		return
	}

	fieldMap, err := label.useCases.Validation(ctxt, labelInfo)

	if err != nil {
		log.Errorf("Validation failed, Error = %s", err.Error())
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, endpoint, method)
		if !valStatus {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusInternalServerError,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	if len(fieldMap) != 0 {
		fields := utils.FieldMapping(fieldMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("Validation failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	if err := label.useCases.CreateLabel(ctxt, labelInfo); err != nil {
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if !valStatus {
			log.Errorf("CreateLabel failed  %s", err.Error())
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusInternalServerError,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
	}

	ctx.JSON(http.StatusOK, gin.H{
		"code":    http.StatusCreated,
		"message": "Labels Added successfully",
	})

}

// ViewAllLabel retrieves and returns label information.
func (label *LabelController) ViewAllLabel(ctx *gin.Context) {

	var params entities.Paramater
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)

	if err := ctx.BindQuery(&params); err != nil {
		log.Errorf("ViewAllLabel failed, Invalid Query parameter data, Error = %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error":   "Invalid JSON data",
			"message": err.Error(),
		})
		return
	}

	method := strings.ToLower(ctx.Request.Method)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	if !isEndpointExists {
		log.Errorf("ViewAllLabel failed, Invalid endpoint %s", contextEndpoints)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	contextError, contextStatusError := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !contextStatusError {
		log.Errorf("ViewAllLabel failed, %s", contextError)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.ContextErr,
			"errors":    nil,
		})
		return
	}
	labelData, metaData, err := label.useCases.ViewAllLabel(ctxt, params)

	if err != nil {
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, endpoint, method)
		if !valStatus {
			log.Errorf("ViewAllLabel failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusInternalServerError,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	// Data retrieved succesfully
	ctx.JSON(http.StatusOK, gin.H{
		"message":  consts.Success,
		"data":     labelData,
		"metadata": metaData,
	})
}

// ViewAllReleases for view all releases under a label
func (label *LabelController) ViewAllReleases(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	contextError, contextStatusError := utils.GetContext[map[string]any](ctx, toolkitconsts.ContextErrorResponses)
	if !contextStatusError {
		log.Errorf("ViewAllLabel failed, %s", contextError)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.ContextErr,
			"errors":    nil,
		})
		return
	}

	// Create a map to store field validation errors
	fieldsMap := map[string][]string{}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()

	// Get context endpoints and check if the requested endpoint exists
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	if !isEndpointExists {
		log.Errorf("ViewAllReleases failed, invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}
	// Get the label ID from the request path parameters
	id := ctx.Param("label_id")
	// Get member ID and partner ID from the jwt token
	memberId, memberValue := ctx.Get("member_id")
	partnerId, partnerValue := ctx.Get("partner_id")

	// Check if labelId is empty
	if id == "" {
		log.Errorf("ViewAllReleases failed, validation error.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.LabelIDNotFound)
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)

		ctx.JSON(int(errorCode), val)
		return

	}
	// Check if memberId is empty
	if memberId == "" || !memberValue {
		log.Errorf("ViewAllReleases failed, validation error.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.MemberIDNotFound)
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)

		ctx.JSON(int(errorCode), val)
		return
	}

	// Check if partnerId is empty
	if partnerId == "" || !partnerValue {
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PartnerIDNotFound)
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)

		ctx.JSON(int(errorCode), val)
		return
	}

	var labelInfo entities.Label

	// Parse pagination parameters from query string
	limitStr := ctx.DefaultQuery("limit", consts.DefaultLimitStr)
	pageStr := ctx.DefaultQuery("page", consts.DefaultPageStr)

	if len(fieldsMap) != 0 {
		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("ViewAllReleases failed, validation error, err = %s	", fields)
		val, _, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)

		ctx.JSON(int(errorCode), val)
		return
	}
	// Convert member ID and partner ID to UUID format
	MemberIdStr := memberId.(string)
	PartnerIdStr := partnerId.(string)

	labelInfo.MemberId = MemberIdStr
	labelInfo.PartnerId = PartnerIdStr

	// Retrieve label data and metadata using the useCases.ViewAllReleases function
	labelData, metaData, err := label.useCases.ViewAllReleases(ctxt, limitStr, pageStr, id)
	if err != nil {
		log.Errorf("ViewAllReleases failed, Error = %s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	// Return the response with data and metadata
	ctx.JSON(http.StatusOK, gin.H{
		"message":  "Successfully retrived details",
		"metadata": metaData,
		"data":     labelData,
	})

}

// UpdateLabel function is to update details of label
func (label *LabelController) UpdateLabel(ctx *gin.Context) {

	ctxt := ctx.Request.Context()
	// member id and partner id is assigned to a variable
	memberId, _ := ctx.Get("member_id")
	partnerId, _ := ctx.Get("partner_id")
	// assigning label id to a variable
	labelId := ctx.Param("id")
	log := logger.Log().WithContext(ctxt)

	contextError, contextStatusError := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if !contextStatusError {
		log.Errorf("UpdateLabel failed, %s", contextError)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.ContextErr,
			"errors":    nil,
		})
		return
	}
	fieldsMap := map[string][]string{}
	var labelInfo map[string]interface{}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		log.Errorf("UpdateLabel failed, invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}
	// Check if memberId is empty
	if memberId == "" {
		log.Errorf("UpdateLabel failed, validation error, invalid track id.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.LabelIDNotFound)
		fields := utils.FieldMapping(fieldsMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("Validation failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}

		ctx.JSON(int(errorCode), val)
		return
	}

	// Check if partnerId is empty
	if partnerId == "" {
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PartnerIDNotFound)
		fields := utils.FieldMapping(fieldsMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("Validation failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	// Check if labelId is empty
	if labelId == "" {
		log.Errorf("UpdateLabel failed, validation error.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.MemberIDNotFound)
		fields := utils.FieldMapping(fieldsMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("Validation failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	if len(fieldsMap) != 0 {
		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("UpdateLabel failed, validation error, err = %s	", fields)
		val, valStatus, errorCode := utils.ParseFields(ctx, toolkitconsts.ValidationErr,
			fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("Validation failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	// Bind JSON data from the request body to labelInfo
	if err := ctx.ShouldBindJSON(&labelInfo); err != nil {
		log.Errorf("UpdateLabel failed, Invalid JSON data, Error = %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	// Update the label using the use case
	if err := label.useCases.UpdateLabel(ctxt, labelId, memberId.(string), partnerId.(string), labelInfo); err != nil {
		log.Errorf("UpdateLabel failed, Error = %s", err.Error())
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, endpoint, method)
		if !valStatus {
			log.Errorf("Validation failed, %s", consts.ParseErr)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}

	// Respond with success message if the update is successful
	ctx.JSON(http.StatusOK, gin.H{
		"message": consts.Success,
	})
}

// DeleteLabel is used to soft delete a label.
func (label *LabelController) DeleteLabel(ctx *gin.Context) {
	// member id and partner id is assigned to a variable
	memberId, _ := ctx.Get("member_id")
	partnerId, _ := ctx.Get("partner_id")
	// assigning label id to a variable
	labelId := ctx.Param("id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	fieldsMap := map[string][]string{}

	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)

	if !isEndpointExists {
		log.Errorf("CreateLabel failed, invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}
	var labelInfo map[string]interface{}

	memberId, memberValue := ctx.Get("member_id")
	partnerId, partnerValue := ctx.Get("partner_id")

	// Check if memberId is empty
	if memberId == "" || !memberValue {
		log.Errorf("DeleteLabel failed, validation error, err = member_id does not exists.")
		utils.AppendValuesToMap(fieldsMap, "common_message", "member_not_found")
		fields := utils.FieldMapping(fieldsMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("DeleteLabel failed")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}

		ctx.JSON(int(errorCode), val)
		return
	}

	// Check if partnerId is empty
	if partnerId == "" || !partnerValue {
		log.Errorf("DeleteLabel failed, validation error, err = partner_id does not exists.")
		utils.AppendValuesToMap(fieldsMap, "common_message", "member_not_found")
		fields := utils.FieldMapping(fieldsMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("DeleteLabel failed")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}

		ctx.JSON(int(errorCode), val)
		return
	}

	// Check if labelId is empty
	if labelId == "" {
		log.Errorf("DeleteLabel failed, validation error, err = label_id does not exists.")
		utils.AppendValuesToMap(fieldsMap, "common_message", "member_not_found")
		fields := utils.FieldMapping(fieldsMap)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("DeleteLabel failed")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}

		ctx.JSON(int(errorCode), val)
		return

	}

	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("DeleteLabel failed, internal server error, err = %s", fields)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("DeleteLabel failed")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}

		ctx.JSON(int(errorCode), val)
		return
	}
	// delete the label using the use case
	if err := label.useCases.DeleteLabel(ctx, labelId, memberId.(string), partnerId.(string), labelInfo); err != nil {
		if err.Error() == "label already inactive" {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   err.Error(),
				"errors":    nil,
			})
			return
		}
		log.Errorf("DeleteLabel failed, Error = %s", err.Error())
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, endpoint, method)
		if !valStatus {
			log.Errorf("DeleteLabel failed")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"errorCode": http.StatusBadRequest,
				"message":   consts.ParseErr,
				"errors":    nil,
			})
			return
		}
		ctx.JSON(int(errorCode), val)
		return
	}
	// Respond with success message if the delete is successful
	ctx.JSON(http.StatusOK, gin.H{
		"message": consts.Success,
	})
}
