package consts

// Constants defining fundamental properties and settings of the application.
const (
	DatabaseType     = "postgres"
	AppName          = "label"
	AcceptedVersions = "v1"
)

// Context setting keys used to manage and access specific values within the application context.
const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
)

// Context setting values
const (
	ContextErrorResponses        = "context-error-response"
	ContextLocallizationLanguage = "lan"
)

// HeaderLocallizationLanguage is the constant for the HTTP request header key used to specify
const (
	HeaderLocallizationLanguage = "Accept-Language"
)

// CacheErrorData is a constant used to set cache error data in the context or elsewhere.
const CacheErrorData = "CACHE_ERROR_DATA"

// ExpiryTime is a constant used to set the expiration time
const ExpiryTime = 180

// keys to retrive error
const (
	ValidationErr     = "validation_error"
	ForbiddenErr      = "forbidden"
	UnauthorisedErr   = "unauthorized"
	NotFound          = "not found"
	InternalServerErr = "internal_server_error"
	Errors            = "errors"
	AllError          = "AllError"
	Registration      = "registration"
	ErrorCode         = "errorCode"
	MemberIDErr       = "member_id"
	ViewLabel         = "view_label"
	EndpointErr       = "Error occured while loading endpoints from service"
	ContextErr        = "Error occured while loading error from service"
	Success           = "Successfully added the details"
	ParseErr          = "Error occured while parsing "
	ContextEndPoints  = "context-endpoints"
)

// Minimum and Maximum length
const (
	MaxLength = 127
	MinLength = 1
)

// Cache Keys
const (
	CacheErrorKey     = "ERROR_CACHE_KEY_LABEL"
	CacheEndpointsKey = "endpoints"
	// Add required constants here…
)

// Error costants
const (
	CommonMessage     = "common_message"
	MemberIDNotFound  = "member_not_found"
	PartnerIDNotFound = "partner_not_found"
	LabelIDNotFound   = "label_not_found"
)

// pagination
const (
	DefaultPageStr  = "1"
	DefaultLimitStr = "10"
)
