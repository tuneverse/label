package main

import (
	"flag"
	"label/app"
)

var (
	runserver = flag.Bool("runserver", false, "This is a string argument for running server")
	migration = flag.Bool("migration", false, "This is a string argument for running migration")
	up        = flag.Bool("up", false, "This is a string argument for running migration up")
	down      = flag.Bool("down", false, "This is a string argument for running migration down")
)

func main() {

	app.Run()

}
