package app

import (
	"context"
	"fmt"
	"label/config"
	"label/internal/consts"
	"label/internal/controllers"
	"label/internal/entities"
	"label/internal/middlewares"
	"label/internal/repo"
	"label/internal/repo/driver"
	"label/internal/usecases"

	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
)

// Run initializes environment configuration, logging, database connection, and API routing.
func Run() {
	// init the env config
	cfg, err := config.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	file := &logger.FileMode{
		LogfileName:  "label.log",      // Log file name.
		LogPath:      "logs",           // Log file path. Add this folder to .gitignore as logs/
		LogMaxAge:    7,                // Maximum log file age in days.
		LogMaxSize:   1024 * 1024 * 10, // Maximum log file size (10 MB).
		LogMaxBackup: 5,                // Maximum number of log file backups to keep.
	}

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: true,           // Include response data in logs.
	}
	// Check if the application is in debug mode.
	var log *logger.Logger

	if cfg.Debug {
		log = logger.InitLogger(clientOpt, file)
	} else {
		db := &logger.CloudMode{

			URL:    cfg.LoggerServiceURL,
			Secret: cfg.LoggerSecret,
		}
		log = logger.InitLogger(clientOpt, db, file)
	}

	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database")
		return
	}

	// here initalizing the router
	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	m := middlewares.NewMiddlewares(cfg)
	api := router.Group("/api")

	api.Use(middleware.LogMiddleware(map[string]interface{}{}))
	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))
	api.Use(m.JwtDecoding())
	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          consts.CacheErrorKey,
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", cfg.LocalisationServiceURL),
		},
	))
	api.Use(middleware.EndpointExtraction(
		middleware.EndPointOptions{
			Cache:            cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:  time.Duration(time.Hour * 24),
			CacheKeyLabel:    consts.CacheEndpointsKey,
			ContextEndPoints: consts.ContextEndPoints,
			EndPointsURL:     fmt.Sprintf("%s/localization/endpointname", cfg.EndpointURL),
		},
	))

	// complete label related initialization
	{

		// repo initialization
		labelRepo := repo.NewLabelRepo(pgsqlDB)

		// initilizing usecases
		labelUseCases := usecases.NewLabelUseCases(labelRepo)

		// initalizing controllers
		labelControllers := controllers.NewLabelController(api, labelUseCases)

		// init the routes
		labelControllers.InitRoutes()
	}

	// run the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	// common middlewares should be added here

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Log().Fatalf("listen: %s\n", err)
		}
	}()
	fmt.Println("Server listening in...", cfg.Port)
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	logger.Log().Printf("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Log().Fatalf("Server Shutdown: %s", err.Error())
	}
	// catching ctx.Done(). timeout of 5 seconds.
	select {
	case <-ctx.Done():
		logger.Log().Printf("timeout of 5 seconds.")
	}
	logger.Log().Printf("Server exiting")
}
